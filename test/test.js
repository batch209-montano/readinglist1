const {longestString, numberOFVowels, withinProfit } = require('../index.js');
//import the assert statements from our chai
const { assert } = require('chai');


//Create 3 test cases in a new test suite in test.js that would check if the functionality of longestString() is correct.

describe('test_the_functionality_of_longest_string',()=>{


    it('test_the_longest_string_of_What_if_we_try_a_super-long_word_such_as_otorhinolaryngology',()=>{

        let findLongestString = longestString("What if we try a super-long word such as otorhinolaryngology");
        assert.equal(findLongestString,19);

    });

    it('test_the_longest_string_of_The_quick_brown_fox_jumps_over_the_lazy_dog', ()=>{

        let findLongestString = longestString("The quick brown fox jumps over the lazy dog");
        assert.equal(findLongestString,5);

    });

    it('test_the_longest_string_of_Google_do_a_barrel_roll', ()=>{

        let findLongestString = longestString("Google do a barrel roll") ;
        assert.equal(findLongestString,6);

    });

});

//Create 3 test cases in a new test suite in test.js that would check if the functionality of numberOfVowels() is correct.

describe('test_the_functionality_of_number_of_vowels',()=>{


    it('test_the_number_of_vowels_in_otorhinolaryngology',()=>{

        let countVowels = numberOFVowels("otorhinolaryngology");
        assert.equal(countVowels,7);

    });

    it('test_the_number_of_vowels_in_brown', ()=>{

        let countVowels = numberOFVowels("brown");
        assert.equal(countVowels,1);

    });

    it('test_the_number_of_vowels_in_Google', ()=>{

        let countVowels = numberOFVowels("Google") ;
        assert.equal(countVowels,3);

    });

});
//Create 3 test cases in a new test suite in test.js that would check if the functionality of withinProfit() is correct.

describe('test_the_functionality_of_within_the_profit',()=>{


    it('test_the_profit_in_50_for_income_30_for_expense_and_2_for_items',()=>{

        let totalProfit = withinProfit(50,10);
        assert.equal(totalProfit,40);

    });

    it('test_the_profit_in_50_for_income_30_for_expense_and_2_for_items',()=>{

        let totalProfit = withinProfit(20000,7000);
        assert.equal(totalProfit,13000);

    });

    it('test_the_profit_in_50_for_income_30_for_expense_and_2_for_items',()=>{

        let totalProfit = withinProfit(500,230);
        assert.equal(totalProfit,270);

    });


});