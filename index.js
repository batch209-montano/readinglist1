//1.   Create a function called longestString() that is able to return the longest string in a given array.

//The argument given to the function must be an array.



function longestString(longestStr) {
    let longestWord = longestStr.split(' ').sort(function(a, b) { return b.length - a.length; });
    return longestWord[0].length;
  }
  
//2.  Create a function called numberOfVowels() that is able to return the number of vowels in a given string.
     
     //The argument given to the function must be a string.

     
function numberOFVowels(countStr) {
    const vowels = ["a", "e", "i", "o", "u"]

        let count = 0;
 
        for (let letter of countStr.toLowerCase()) {
            if (vowels.includes(letter)) {
                count++;
            }
        }
    
        // return number of vowels
        return count;
    }

//3. Create a function called withinProfit() that is able to return a boolean if the given expense and income returns a profit.
    
function withinProfit(income,expense) {

    let profit = (income - expense);

    if (profit > expense) {
        return profit;
    } 
    
}

module.exports = {
    longestString : longestString,
    numberOFVowels :numberOFVowels,
    withinProfit : withinProfit
}